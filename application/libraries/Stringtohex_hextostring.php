<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stringtohex_hextostring extends CI_Controller {

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function Hex2str($hexString = "")
	{
		//return $this->CI->
		// Remove spaces if the hex string has spaces
	    $hex = str_replace(' ', '', $hexString);
	    return hex2bin($hex);
	}

	public function Str2hex($string = "")
	{
		$hexstr = unpack('H*', $string);
  		return array_shift($hexstr);
	}

	

}

/* End of file Messages.php */
/* Location: ./application/controllers/Messages.php */