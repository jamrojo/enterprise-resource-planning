<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller {

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function message_error($message = "")
	{
		return $this->CI->session->set_flashdata('error', '<div class="alert alert-danger shadow-lg animated fadeInUp notifications"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b><i class="fas fa-exclamation-circle"></i> '.$message.'</b></div>');
	}

	public function message_success($message = "")
	{
		return $this->CI->session->set_flashdata('success', '<div class="alert alert-success shadow-lg animated fadeInUp notifications"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b><i class="fas fa-exclamation-circle"></i> '.$message.'</b></div>');
	}

	public function message_custom($class = "alert-default", $message = "")
	{
		return $this->CI->session->set_flashdata('custom', '<div class="alert '.$class.' shadow-lg animated fadeInUp notifications"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b><i class="fas fa-exclamation-circle"></i> '.$message.'</b></div>');
	}

}

/* End of file Messages.php */
/* Location: ./application/controllers/Messages.php */