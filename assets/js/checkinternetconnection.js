function showStatus(online) {
  const statusEl = document.querySelector('.network-status');

  if (!online) {
    statusEl.classList.remove('hidden')
    statusEl.classList.add('warning');
    statusEl.innerText = 'Has perdido tu conexión a internet, te recomendamos esperar a que se reestablezca para que no pierdas tus datos.'; 
  }
  else{
    statusEl.classList.remove('warning')
    statusEl.classList.add('success');
    statusEl.innerText = 'Ya estas conectado a internet nuevamente!'; 
    setTimeout(function(){
      statusEl.classList.add('hidden');
    },5000)
  }
}

window.addEventListener('load', () => {
  // 1st, we set the correct status when the page loads
  navigator.onLine ? showStatus(true) : showStatus(false);

  // now we listen for network status changes
  window.addEventListener('online', () => {
    showStatus(true);
  });

  window.addEventListener('offline', () => {
    showStatus(false);
  });
});