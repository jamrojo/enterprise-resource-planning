$(document).ready(function(){
	/*
	main slider
	 */
	var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"), $thumbnailsSlider = $(".gallery-slider__thumbnails>div");
	// images options
	$imagesSlider.slick({
		dots:true,
		speed:300,
		slidesToShow:1,
		slidesToScroll:1,
		cssEase:'linear',
		fade:true,
		autoplay:true,
		draggable:false,
		arrows:true,
		nextArrow: '<button class="next-arrow"><i class="fas fa-angle-right fa-2x"></i></button>',
  		prevArrow: '<button class="prev-arrow"><i class="fas fa-angle-left fa-2x"></i></button>',
	});

	var $caption = $('.gallery-slider .caption');
	var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
	updateCaption(captionText);
	$imagesSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$caption.addClass('hide');
	});
	$imagesSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
		captionText = $('.gallery-slider__images .slick-current img').attr('alt');
		updateCaption(captionText);
	});

	function updateCaption(text) {
		if (text === '') {
			text = '&nbsp;';
		}
		$caption.html(text);
		$caption.removeClass('hide');
	}

	$('.multi-slick-items').slick({
		dots: true,
		infinite: true,
		speed: 600,
		slidesToShow: 4,
		autoplay:true,
		slidesToScroll: 4,
		autoplaySpeed:4000,
		responsive: [
			{
				breakpoint:640,
				settings: {
					slidesToShow:1,
					slidesToScroll:1,
					arrows:false,
					dots:false
				}
			},
			{
				breakpoint:1024,
				settings:{
					slidesToShow:2,
					slidesToScroll:2,
					arrows:false,
					dots:false
				}
			}
		]
	})
	$('#slider-marcas').slick({
		dots: true,
		infinite: true,
		speed: 600,
		slidesToShow: 5,
		slidesToScroll: 5,
		responsive: [
			{
				breakpoint:640,
				settings: {
					slidesToShow:1,
					slidesToScroll:1,
					arrows:false,
					dots:false
				}
			},
			{
				breakpoint:1024,
				settings:{
					slidesToShow:2,
					slidesToScroll:2,
					arrows:false,
					dots:false
				}
			}
		]
	})
})